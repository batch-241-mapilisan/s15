console.log("Hello World!");

// It will comment parts of the code that gets ignored by the language

/*
    There are two types of comments
    1. the single line comment denoted by two slashes
    2. The multi-line comment denoted by slash and asterisk
*/

// [Section] Syntax, statements
    // Statements in programming are instructions that we tell the computer to perform
    // JS statements usually end with semicolon (;)
    // Semicolons are not required in JS but we will use it to help us train to locate where statement ends.
    // A syntax in programming, it is the set of rules that we describes statements must be constructed
    // All lines/blocks of code should be written in specific manner to work. This is due to how these codes where initially programmed to function in a certain manner.


// [Section] Variable
    // Variables are used to contain data
    // Any information that is used by an application is stored in what we call the memory
    // When we create variables, certain portions of device's memory is given a name that we call variables

    // This makes it easier for us to associate information stored in our devices to actual "names" information

    // Declaring Variables
    // Declaring Variables - it tells our devices that a variable name is created and is ready to store data.
        // Syntax
            // let/const variableName;

    let myVariable = "Ada Lovelace"
    let variable;
    // This will caused an undefined variable because the declared variable does not have initial value
    console.log(variable);
    // const keyword is used when the value of the variable wont change.
    const constVariable = "John Doe"
    // console.log() is useful for printing values of variables or certain results of code into the browser's console
    console.log(myVariable);
    
    /* 
        Guides in writing Variables
            1.  use the let keyword followed the variable name of your choice and use the assignment operator (=) to assign value.
            2.  Variable names should start with lowercase character, use camelCasing for multiple words.
            3.  For constant variables, use the 'const keyword'
                Note: If we use const keyword in declaring a variable, we cannot change the value of its variable.
            4. Variable names should be indicative (descriptive) of the value being stored to avoid confusing.
    */

    // Declare and initialize
        // Initializing variables - the instance when a variable is given its first/initial value
        // Syntax:
            // let/const variableName = intial value;
    
    let productName = "desktop computer";
    // Declaration
    let desktopName;
    // Initialization of the value of variable desktopName
    desktopName = "Dell";
    console.log(desktopName);

    // Reassigning value
        // Syntax: variableName = newValue
    productName = "Personal Computer";
    console.log(productName);

    const name = "Chris";
    console.log(name);
    // This reassignment will cause an error since we cannot change/reassign the initial value of constant variable.

    // name = "Topher";
    // console.log(name);

    // This will cause an error on our code because the productName variable is already taken.
    // let productName = "Laptop";

    // var vs let/const
        // some of you may wonder why we used let and const keyword in declaring a variable when we search online, we usually see var.

        // var - is also used in declaring variable but var is an ecmaScript1 feature [(1997)]

    let lastName;
    lastName = "Mortel";
    console.log(lastName);

/* 
    Using var, bad practice.
    batch = "Batch 241";

    var batch;
    console.log(batch);
*/

///////////////////////////////
// let/const local/global scope
/*
    Scope essentially means where these variables are available for use

    let/const are block scope

    A block is a chunk of code bounded by {}.
 */

// let outerVariable = "Hello";

// {
//     let innerVariable = "Hello Again";
// }

// console.log(outerVariable);
// console.log(innerVariable);

// const outerVariable = "Hello";
// {
//     const innerVariable = "Hello Again";
// }

// console.log(outerVariable);
// console.log(innerVariable);

var outerVariable = "Hello";
{
    var innerVariable = "Hello Again";
}

console.log(outerVariable);
console.log(innerVariable);

// Multiple variable declarations

let productCode = "DC017", productBrand = "Dell";
console.log(productCode);
console.log(productBrand);

// Using a variable with a reserved keyword
// const let = "Hello";
// console.log(let);

// [SECTION] Data Types
// Strings
/*
    Strings are a series of characters the create a word, a phrase, a sentence or anything related to creating a text
    String in JavaScript can be written using either a single ('') or double ("") quote
*/

let country = "Philippines";
let province = 'Metro Manila';
console.log(country);
console.log(province);

// Concatonate strings
// Multiple string values can be combined to create a single string using "+" symbol
let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// Declaring a string using an escape character
let message = 'John\'s employees went home early';
console.log(message);

message = "John's employees went home early";
console.log(message);

//"\n" - refers to creating a new line in between text
let mailAddress = "Metro Manila \n\nPhilippines"
console.log(mailAddress);

// Numbers
// Integer/Whole Number
let headCount = 27;
console.log(headCount);

// Decimal Number
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and strings
console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normall used to store values relating to the state of certain things
// true or false
let isMarried = false;
let inGoodConduct = true;
console.log(typeof isMarried);
console.log(typeof inGoodConduct);

// Arrays
// Arrays are a special kind data type that's use to store multiple values

/*
    Syntax:
        let/const arrayName = [elementA, elementB, ...];
*/

// similar data types
let grades = [98, 92.1, 90.1, 94.7];
console.log(grades);
// Array is a special kind of object
console.log(typeof grades);

// different data type
let details = ["John", 32, true];
console.log(details);

// Objects are another special kind of data type that's used to mimic real world objects/items

/*
    Syntax:
        let/const objectName = {
            propertyA: value,
            propertyB: value
        }
*/

let person = {
    firstName: "John",
    lastName: "Smith",
    age: 32,
    isMarried: false,
    contact: ["+639123456789", "8123-4567"],
    address: {
        houseNumber: "345",
        city: "Manila"
    }
}

console.log(person);

let myGrades = {
    firstGrading: 98,
    secondGrading: 92.1,
    thirdGrading: 90.1,
    fourthGrading: 94.7,
}

/*
    Constant Objects and Arrays

    We can change the element of an array to a constant variable
*/

const anime = ["one piece", "one punch man", "your lie in april"];
console.log(anime);
// arrayName[indexNumber]
anime[0] = ['kimetsu no yaiba'];
console.log(anime);

// const anime1 = ["one piece", "one punch man", "your lie in april"];
// console.log(anime1);
// anime1 = ['kimetsu no yaiba'];
// console.log(anime1);

// Null
// It is used to intentionally express the absence of a value in a variable declaration/initialization
let spouse = null;
console.log(spouse);

let myNumber = 0;
let myString = "";

// Undefined
// Represent the state of a variable that has been declared but without an assigned value
let fullName;
console.log(fullName);
let firstName = "John Michael";
let lastName = "Mapilisan";
let age = 24;
const hobbies = ['Watching Anime', 'Coding', 'Eating'];
const workAddress = {
    houseNumber: '1',
    street: 'New Street',
    city: 'Newport City',
    state: 'New State'
}

console.log(`First Name: ${firstName}`);
console.log(`Last Name: ${lastName}`);
console.log(`Age: ${age}`);
console.log("Hobbies:");
console.log(hobbies);
console.log("Work Address:");
console.log(workAddress);

let fullName = "Steve Rogers";
let currentAge = 40;
const friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
const fullProfile = {
    username: 'captain_america',
    fullname: fullName,
    age: currentAge,
    isActive: false
};
let bestFriend = "Bucky Barnes";
let foundIn = "Arctic Ocean";

console.log(`My full name is: ${fullName}`);
console.log(`My current age is: ${currentAge}`);
console.log("My friends are:");
console.log(friends);
console.log("My Full Profile");
console.log(fullProfile);
console.log(`My bestfriend is: ${bestFriend}`);
console.log(`I was found frozen in: ${foundIn}`);
